Release
-------
| Version | 
| ------- | 
|1.0.0|

Requirements
------------
* Java 1.8 or newer

Webcrawler
================
Crawler gets input from Stdin and searches Google for top results. Subsequently downloads the top results and lists all used Javascript Libraries.

### Gradle
In order to start the project, open it directly in idea. The idea-plugin will do the rest
