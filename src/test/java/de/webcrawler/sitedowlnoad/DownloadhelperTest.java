package de.webcrawler.sitedowlnoad;

import de.webcrawler.exceptions.HttpDownloadException;
import de.webcrawler.sitedownload.Downloadhelper;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

@RunWith(PowerMockRunner.class)
@PrepareForTest({Jsoup.class})
public class DownloadhelperTest {

    @Test(expected = HttpDownloadException.class)
    public void shouldThrowIOException() throws Exception {
        Connection connection = Mockito.mock(Connection.class);
        Mockito.when(connection.get()).thenThrow(new IOException("test"));
        PowerMockito.mockStatic(Jsoup.class);

        PowerMockito.when(Jsoup.connect(Mockito.anyString())).
                thenReturn(connection);
        Mockito.when(connection.userAgent(Mockito.any())).
                thenReturn(connection);

        Downloadhelper downloadhelper = new Downloadhelper();
        downloadhelper.getSearchResults("Test");
    }

    @Test
    public void getJavaSriptLibrariesTest() throws IOException, HttpDownloadException {
        Connection connection = Mockito.mock(Connection.class);
        Mockito.when(connection.get()).thenReturn(generateDocumentFromResource("document_javascript_libraries"));
        PowerMockito.mockStatic(Jsoup.class);

        PowerMockito.when(Jsoup.connect(Mockito.anyString())).
                thenReturn(connection);
        Mockito.when(connection.userAgent(Mockito.any())).
                thenReturn(connection);

        Downloadhelper downloadhelper = new Downloadhelper();
        LinkedList<String> input = new LinkedList<>();
        input.add("https://www.test.de/");
        List results = downloadhelper.getJavascriptLibraries(input);


        LinkedList<String> expectedResults = new LinkedList<>();
        expectedResults.add("/scripts/base.min;v63715981170.js");
        expectedResults.add("/scripts/modules.min;v63715981170.js");
        expectedResults.add("/scripts/webtrekk/webtrekk_cookieControl.min;v63713982210.js");
        expectedResults.add("/scripts/webtrekk/webtk.min;v63713982210.js");

        Assert.assertEquals(results, expectedResults);

    }

    @Test
    public void getSearchResultsTest() throws IOException, HttpDownloadException {
        Connection connection = Mockito.mock(Connection.class);
        Mockito.when(connection.get()).thenReturn(generateDocumentFromResource("document_google_search"));
        PowerMockito.mockStatic(Jsoup.class);

        PowerMockito.when(Jsoup.connect(Mockito.anyString())).
                thenReturn(connection);
        Mockito.when(connection.userAgent(Mockito.any())).
                thenReturn(connection);

        Downloadhelper downloadhelper = new Downloadhelper();
        List results = downloadhelper.getSearchResults("Test");


        LinkedList<String> expectedResults = new LinkedList<>();
        expectedResults.add("https://www.test.de/");
        expectedResults.add("https://www.chip.de/tests");
        expectedResults.add("https://de.wikipedia.org/wiki/Test_(Zeitschrift)");
        expectedResults.add("https://www.computerbild.de/tests/");
        expectedResults.add("https://www.sueddeutsche.de/thema/Test");
        expectedResults.add("https://www.oekotest.de/tests");
        expectedResults.add("https://www.computerbase.de/2020-02/amd-ryzen-threadripper-3990x-test/");

        Assert.assertEquals(results, expectedResults);

    }

    private Document generateDocumentFromResource(String name) throws IOException {
        return Jsoup.parse(new File(getClass().getClassLoader().getResource(name).getFile()), "UTF-8");
    }
}
