package de.webcrawler.sitedownload;


import de.webcrawler.exceptions.HttpDownloadException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Downloadhelper {

    private static Logger LOGGER;

    static {
        System.setProperty("java.util.logging.SimpleFormatter.format",
                "[%1$tF %1$tT] [%4$-7s] %5$s %n");
        LOGGER = Logger.getLogger(Downloadhelper.class.getName());
    }


    public static final String GOOGLE_SEARCH_URL = "https://www.google.com/search";
    public static final String USERAGENT = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36";

    /**
     * Get a list of the Google Search Results based on the search term provided
     *
     * @param searchterm Search term for the google search
     * @return List fo search results
     * @throws HttpDownloadException Thrown, if the site cannot be downloaded
     */
    public List getSearchResults(String searchterm) throws HttpDownloadException {
        String searchURL = GOOGLE_SEARCH_URL + "?q=" + searchterm;
        LinkedList<String> resultUrlList = new LinkedList<>();


        // Google Search results are currently saved under the div class "r" in an "a" element.
        // This can change anytime and have to be adjusted accordingly
        Elements results = downloadHTML(searchURL, USERAGENT).select("div.r > a");

        for (Element result : results) {
            resultUrlList.add(result.attr("href"));
        }

        return resultUrlList;

    }

    /**
     * Download Website from the URL provided.
     *
     * @param url       Site to download
     * @param useragent The Useragent to use. Goolge only fulfill requests when the useragent is set.
     * @return Jsoup Document that contains the HTML
     * @throws HttpDownloadException Thrown, if the site cannot be downloaded
     */
    private Document downloadHTML(String url, String useragent) throws HttpDownloadException {
        try {
            Document document = Jsoup.connect(url).userAgent(USERAGENT).get();
            return document;
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, "Unable to download HTTP page for url " + url + " and Useragent: " + USERAGENT);
            throw new HttpDownloadException("Unable to download HTTP page", e);
        }
    }

    /**
     * Get the javascript libraries form the urls provided.
     * Currently the method just searches for <script> tags in the HTML and looks at the <src> attribute.
     * <p>
     * This is not the most elegant way nor the best way to identify javascript libraries.
     * But given the time limit of 3 hours a hacky work around.
     *
     * @param urlsToFetch List of urls to fetch the javascript libraries
     * @return List of javascript libraries used on the site
     * @throws HttpDownloadException Thrown, if the site cannot be downloaded
     */
    public List<String> getJavascriptLibraries(List<String> urlsToFetch) throws HttpDownloadException {
        LinkedList<String> libraries = new LinkedList<>();
        for (String url : urlsToFetch) {
            LOGGER.info("Get libraries for URL: " + url);
            Document doc = downloadHTML(url, USERAGENT);

            Elements script = doc.getElementsByTag("script");
            for (Element element : script) {
                String scriptName = element.attr("src");

                // sometimes <script> does not contain a <src> attribute
                if (scriptName != null && !scriptName.isEmpty()) {
                    libraries.add(scriptName);
                }
            }


        }
        return libraries;
    }
}
