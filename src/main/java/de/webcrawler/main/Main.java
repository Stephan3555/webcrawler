package de.webcrawler.main;

import de.webcrawler.exceptions.HttpDownloadException;
import de.webcrawler.sitedownload.Downloadhelper;
import de.webcrawler.util.Utils;

import java.util.List;


public class Main {


    public static void main(String[] args) throws HttpDownloadException {
        Downloadhelper downloadhelper = new Downloadhelper();
        List<String> googleResults = downloadhelper.getSearchResults(Utils.READFROMCONSOLE());
        List<String> javaScriptLibraries = downloadhelper.getJavascriptLibraries(googleResults);

        Utils.printResults(javaScriptLibraries);

    }


}
