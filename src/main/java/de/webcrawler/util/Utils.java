package de.webcrawler.util;


import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public abstract class Utils {
    private static Logger LOGGER;

    static {
        System.setProperty("java.util.logging.SimpleFormatter.format",
                "[%1$tF %1$tT] [%4$-7s] %5$s %n");
        LOGGER = Logger.getLogger(Utils.class.getName());
    }


    /**
     * Reads Input from Stdin
     *
     * @return Entered Searchterm
     */
    public static String READFROMCONSOLE() {
        LOGGER.info("Please type in Searchterm and confirm with <ENTER>:");
        Scanner in = new Scanner(System.in);
        String searchterm = in.nextLine();
        in.close();
        return searchterm;
    }

    /**
     * Performes a GroupBy Operation on the List to identify duplicates and counts the elements.
     *
     * @param javaScriptLibraries List with javascript libraries to count
     * @return Map of javascript libraries and their occurence
     */
    private static Map<String, Long> countOccurences(List<String> javaScriptLibraries) {
        return javaScriptLibraries.stream().collect(Collectors.groupingBy(e -> e, Collectors.counting()));
    }

    /**
     * Pretty print a list
     *
     * @param list The list to print
     */
    public static void printResults(List<String> list) {

        Map<String, Long> counts = countOccurences(list);
        LOGGER.info("==================================================================================");
        LOGGER.info("=                                 RESULTS:                                       =");
        LOGGER.info("==================================================================================");
        counts.forEach((key, value) -> LOGGER.info(key + " --> " + value));
        LOGGER.info("==================================================================================");
    }
}
