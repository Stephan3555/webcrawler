package de.webcrawler.exceptions;

public class HttpDownloadException extends Exception {

    public HttpDownloadException(String message, Exception e) {
        super(message, e);
    }
}
